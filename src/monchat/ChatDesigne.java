package monchat;
import java.io.IOException;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

public class ChatDesigne extends javax.swing.JFrame {  
    private Socket socket;
    ChatBackground myChat;
    public String myMessage;
    private Thread principalThread;
    String userName;
    
    private void Server(){
        BackgroundServer myServer = new BackgroundServer(this, Integer.valueOf(this.jtxtport.getText()));
        principalThread = new Thread(myServer);
        principalThread.start(); 
    }

    public ChatDesigne() {
        initComponents();
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        jLabel1 = new javax.swing.JLabel();
        jtxtaddresse = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jtxtport = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        joutput = new javax.swing.JTextArea();
        jButton1 = new javax.swing.JButton();
        jmessage = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        jsend = new javax.swing.JButton();
        radioserver = new javax.swing.JRadioButton();
        radioclient = new javax.swing.JRadioButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("My Chat");
        setAutoRequestFocus(false);
        setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));

        jLabel1.setText("IP Addresse");

        jLabel2.setText("Port");

        jtxtport.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jtxtportActionPerformed(evt);
            }
        });

        joutput.setEditable(false);
        joutput.setColumns(20);
        joutput.setRows(5);
        jScrollPane1.setViewportView(joutput);

        jButton1.setText("Connect");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jmessage.setEnabled(false);
        jmessage.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jmessageActionPerformed(evt);
            }
        });

        jLabel3.setText("Message");

        jsend.setText("Send");
        jsend.setEnabled(false);
        jsend.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jsendActionPerformed(evt);
            }
        });

        buttonGroup1.add(radioserver);
        radioserver.setText("Server");
        radioserver.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                radioserverActionPerformed(evt);
            }
        });

        buttonGroup1.add(radioclient);
        radioclient.setText("Client");
        radioclient.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                radioclientActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 62, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jmessage, javax.swing.GroupLayout.PREFERRED_SIZE, 265, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jsend, javax.swing.GroupLayout.PREFERRED_SIZE, 87, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 62, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jtxtaddresse, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jtxtport, javax.swing.GroupLayout.PREFERRED_SIZE, 98, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 265, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(radioclient, javax.swing.GroupLayout.PREFERRED_SIZE, 87, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(radioserver, javax.swing.GroupLayout.PREFERRED_SIZE, 87, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jButton1)
                                .addGap(64, 64, 64)))))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(11, 11, 11)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jtxtaddresse, javax.swing.GroupLayout.PREFERRED_SIZE, 19, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 19, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jtxtport, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton1))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(radioserver)
                        .addGap(18, 18, 18)
                        .addComponent(radioclient))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 228, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jmessage, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3)
                    .addComponent(jsend, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(38, Short.MAX_VALUE))
        );

        jsend.getAccessibleContext().setAccessibleDescription("");

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jtxtportActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jtxtportActionPerformed

    }//GEN-LAST:event_jtxtportActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        if(radioclient.isSelected()){
            connectClient();
        }
        else if(radioserver.isSelected()){
            Server();
            connectServer();
        }
        else{
            JOptionPane.showMessageDialog(this, "Select server or client", "Error message",JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jmessageActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jmessageActionPerformed

    }//GEN-LAST:event_jmessageActionPerformed

    private void jsendActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jsendActionPerformed
        sendfromclient();
        jmessage.setText("");
    }//GEN-LAST:event_jsendActionPerformed

    private void radioclientActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_radioclientActionPerformed
        jtxtaddresse.setEnabled(true);
    }//GEN-LAST:event_radioclientActionPerformed

    private void radioserverActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_radioserverActionPerformed
        jtxtaddresse.setEnabled(false);
    }//GEN-LAST:event_radioserverActionPerformed

    public static void main(String args[]) {
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ChatDesigne.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ChatDesigne.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ChatDesigne.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ChatDesigne.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        
        
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new ChatDesigne().setVisible(true);
            }
        });
        
    }
    
    public void connectClient(){
        try {
            this.socket = new Socket(this.jtxtaddresse.getText(), Integer.valueOf(this.jtxtport.getText()));
            myChat = new ChatBackground(socket, this);
            
            Thread t1 = new Thread(myChat);
            t1.start();
            
            if(myChat.socket.isConnected()){
                JOptionPane.showMessageDialog(this, "Connection establish to IP:"+this.jtxtaddresse.getText() + " on Port:" + this.jtxtport.getText(), "Connection info",JOptionPane.INFORMATION_MESSAGE); 
                enableSend();
            }
            else if(jtxtport.getText() == ""){
                JOptionPane.showMessageDialog(this, "One or several fields is/are empty", "Connection info",JOptionPane.WARNING_MESSAGE);
            }
            else{
                JOptionPane.showMessageDialog(this, "Connection failed!", "Connection info",JOptionPane.WARNING_MESSAGE);
            }       
        } 
        catch (IOException ex) {
            Logger.getLogger(ChatDesigne.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(this, "Programme error 1", "Error message",JOptionPane.ERROR_MESSAGE);
        }
    }
    
    public void connectServer(){
        try {
            
            this.socket = new Socket("localhost", Integer.valueOf(this.jtxtport.getText()));
            myChat = new ChatBackground(socket, this);
            
            Thread t2 = new Thread(myChat);
            t2.start();
            
            if(myChat.socket.isConnected() == true){
                JOptionPane.showMessageDialog(this, "Server established to port " + String.valueOf(Integer.valueOf(this.jtxtport.getText())) + ", " + userName, "Connection info",JOptionPane.INFORMATION_MESSAGE); 
                enableSend();
            }
            else if(jtxtaddresse.getText() == "" || jtxtport.getText() == ""){
                JOptionPane.showMessageDialog(this, "One or several fields is/are empty", "Connection info",JOptionPane.WARNING_MESSAGE);
            }
            else{
                JOptionPane.showMessageDialog(this, "Connection failed", "Connection info",JOptionPane.WARNING_MESSAGE);
            }
            
        } catch (IOException ex) {
            Logger.getLogger(ChatDesigne.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(this, "Programme error 2", "Error message",JOptionPane.ERROR_MESSAGE);
        }
    }
    
    public void enableSend(){
        jtxtaddresse.setText("");
        jtxtport.setText("");
        jtxtaddresse.setEnabled(false);
        jtxtport.setEnabled(false);
        jsend.setEnabled(true);
        jmessage.setEnabled(true);
        jButton1.setEnabled(false);
    }
    
    public void sendfromclient(){
        myChat.messagetoSend = jmessage.getText();
        this.myChat.requestToSend = true;  
        
        if(!jmessage.getText().equals("")){
            updateTextArea("Me:\t" + jmessage.getText());
        }      
    }
    
    public void updateTextArea(String text){
        if(!text.equals("n/a") && !text.equals("")){
            joutput.append(text + "\n");
        }
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextField jmessage;
    private javax.swing.JTextArea joutput;
    private javax.swing.JButton jsend;
    private javax.swing.JTextField jtxtaddresse;
    private javax.swing.JTextField jtxtport;
    private javax.swing.JRadioButton radioclient;
    private javax.swing.JRadioButton radioserver;
    // End of variables declaration//GEN-END:variables
}
