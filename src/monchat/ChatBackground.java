package monchat;

import java.util.Scanner;
import java.io.IOException;
import java.io.PrintStream;
import java.net.Socket;
import java.net.ServerSocket;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

public class ChatBackground implements Runnable {
    Socket socket;
    ServerSocket socketServer;
    ChatDesigne ui;
    PrintStream writer;
    Scanner reader;
    String messagetoSend = "n/a";
    String messageRecieved = "n/a";
    String lastMessage = "n/a";
    boolean requestToSend = false;
    boolean requestToRecieve;
    
    public ChatBackground(Socket soc, ChatDesigne ui){
        try {
            this.socket = soc;
            this.ui = ui;
            this.requestToRecieve = false;
            reader = new Scanner(socket.getInputStream());
            writer = new PrintStream(socket.getOutputStream());
        } 
        catch (IOException ex) {
            Logger.getLogger(ChatBackground.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @Override
    public void run(){  
        try {
            if(ui.userName != ""){
                String nom = "$$$$" + String.valueOf(ui.userName);
                this.writer.println(nom);
            }
            else{
                this.writer.println("$$$$No name");
            }
            
            while(true){ 
                synchronized(this){
                    if(this.requestToSend){                        
                        this.writer.println(this.messagetoSend);
                        this.lastMessage = this.messagetoSend;
                        messagetoSend = "n/a";
                        this.requestToSend = false;
                    }
                    else{
                        writer.println("n/a");
                    }
                }
                
                synchronized(this){
                    this.messageRecieved = this.reader.nextLine();
                   
                    if(!this.messageRecieved.equals("n/a") && !this.messageRecieved.equals("") && !this.messageRecieved.contains(lastMessage) /*&& !this.messageRecieved.contains("$$$$")*/)
                    {
                        this.ui.updateTextArea(this.messageRecieved);
                        lastMessage = "n/a";
                    }     
                }           
            }
        } 
        catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage(), "Something went wrong",JOptionPane.ERROR_MESSAGE);
        }
    }
}