package monchat;

import java.io.IOException;
import java.net.Socket;
import java.net.ServerSocket;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

public class AcceptConnection implements Runnable {
    int port =0;
    ServerSocket socketServer;
    Socket socket;
    BackgroundServer ui;
    
    public AcceptConnection(BackgroundServer ui, int port){
        this.ui = ui;
        this.port = port;
     
        try {
            socketServer = new ServerSocket(port);
        } catch (IOException ex) {
            Logger.getLogger(AcceptConnection.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @Override
    public void run(){
        while(true){
            try {
                socket = socketServer.accept();
                ui.socket = socket;

            } catch (IOException ex) {
                Logger.getLogger(AcceptConnection.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
