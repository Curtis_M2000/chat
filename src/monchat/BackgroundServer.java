package monchat;

import java.io.IOException;
import java.net.Socket;
import java.net.ServerSocket;
import java.util.Scanner;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class BackgroundServer implements Runnable {
    Scanner reader;
    PrintStream writer;
    ChatDesigne ui;
    ServerSocket socketServer = null;
    public Socket socket = null;
    ArrayList<Socket> tabSocket = new ArrayList<Socket>();
    ArrayList<String> tabName = new ArrayList<String>();
    String message = "n/a";
    int port = 01;
    Thread myThread;
    AcceptConnection myConnection;
    
    public BackgroundServer(ChatDesigne ui, int port){
        this.port = port;
        this.ui = ui;
        myConnection = new AcceptConnection(this, port);
        myThread = new Thread(myConnection);
        myThread.start();
    }
    
    public void help(String m){
        if(!m.contains("n/a")){
            System.out.println(m);
        }
    }
    
    @Override
    public void run(){
        
        while(true){  
            if(socket != null){
                tabSocket.add(socket);
                System.out.println("the size of your arraylist is : " + String.valueOf(tabSocket.size()));
                socket = null;            
            }
            
            synchronized(this){
                for(int i=0; i<tabSocket.size(); i++){
                    try {
                        reader = new Scanner(tabSocket.get(i).getInputStream());
                        message = reader.nextLine();  
 
                        if(!message.equals("n/a") && !message.equals("") && !message.contains("$$$$")){
                            for(int j=0; j<tabSocket.size(); j++){
                                writer = new PrintStream(tabSocket.get(j).getOutputStream()); 
                                help("j : " + String.valueOf(j));
                                help(String.valueOf(tabName.get(i)));
                                writer.println(String.valueOf(tabName.get(i)) + ":\t" + message);   
                                writer.println("n/a");
                            }
                            message = "n/a";
                        }
                        
                        else if(message.contains("$$$$")){
                            tabName.add(message.substring(4));
                            help(message.substring(4));
                            for(int j=0; j<tabSocket.size(); j++){
                                writer = new PrintStream(tabSocket.get(j).getOutputStream()); 
                                writer.println("n/a");
                            }
                        }
                        
                        else{
                            writer = new PrintStream(tabSocket.get(i).getOutputStream());   
                            writer.println("n/a");
                        }
                    
                    } catch (IOException ex) {
                        Logger.getLogger(BackgroundServer.class.getName()).log(Level.SEVERE, null, ex);
                    }                      
                }
            }
        }
    }
}
                
