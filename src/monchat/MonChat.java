package monchat;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

public class MonChat {
    public static void main(String[] args) {   
        try {
            InetAddress adrs;
            adrs = InetAddress.getLocalHost();
            
            ChatDesigne chat = new ChatDesigne();
            chat.setTitle("Chat V3.0. Your IP Addrese is: " + adrs.getHostAddress());
            chat.setVisible(true);
            chat.userName = String.valueOf(JOptionPane.showInputDialog("What's your name? "));
        } catch (UnknownHostException ex) {
            Logger.getLogger(MonChat.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}